# Golang testing example

[![coverage report](https://gitlab.com/phuonghuynh-eng/go-testing-example/badges/master/coverage.svg)](https://gitlab.com/phuonghuynh-eng/go-testing-example/commits/master)


## How to run?
To run this example, follow command:

```
go run .
```

## How to test & get coverage?

```
go test -v -cover -tags test
```

